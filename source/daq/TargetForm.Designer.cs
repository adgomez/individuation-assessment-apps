﻿namespace Target
{
    partial class TargetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.MySerialPort = new System.IO.Ports.SerialPort(this.components);
            this.TaskTimer = new System.Windows.Forms.Timer(this.components);
            this.SummaryTab = new System.Windows.Forms.TabPage();
            this.extSummary = new System.Windows.Forms.CheckedListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.flexSummary = new System.Windows.Forms.CheckedListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.calSummary = new System.Windows.Forms.CheckedListBox();
            this.MeasurementTab = new System.Windows.Forms.TabPage();
            this.dataChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.fingerGroupBox = new System.Windows.Forms.GroupBox();
            this.FingerCheckP = new System.Windows.Forms.RadioButton();
            this.FingerCheckR = new System.Windows.Forms.RadioButton();
            this.FingerCheckM = new System.Windows.Forms.RadioButton();
            this.FingerCheckI = new System.Windows.Forms.RadioButton();
            this.FingerCheckT = new System.Windows.Forms.RadioButton();
            this.measurementGroupBox = new System.Windows.Forms.GroupBox();
            this.measFlexCheck = new System.Windows.Forms.RadioButton();
            this.measExtCheck = new System.Windows.Forms.RadioButton();
            this.measPictureBox = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.indStartButton = new System.Windows.Forms.Button();
            this.measurementTitle = new System.Windows.Forms.Label();
            this.CalibrationTab = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.extRadioB = new System.Windows.Forms.RadioButton();
            this.flexRadioB = new System.Windows.Forms.RadioButton();
            this.neutralRadioB = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.calPictureBox = new System.Windows.Forms.PictureBox();
            this.calStartButton = new System.Windows.Forms.Button();
            this.CalibrationTitle = new System.Windows.Forms.Label();
            this.Right1 = new System.Windows.Forms.Label();
            this.Left1 = new System.Windows.Forms.Label();
            this.calGloveStatus = new System.Windows.Forms.Label();
            this.calGloveStatusLabel = new System.Windows.Forms.Label();
            this.GameDisplay = new System.Windows.Forms.PictureBox();
            this.SettingsTab = new System.Windows.Forms.TabPage();
            this.DataSettings = new System.Windows.Forms.GroupBox();
            this.TimeoutUnits = new System.Windows.Forms.Label();
            this.BasicSounds = new System.Windows.Forms.RadioButton();
            this.AudioOn = new System.Windows.Forms.Label();
            this.BlocksNumeric = new System.Windows.Forms.NumericUpDown();
            this.TimeoutLabel = new System.Windows.Forms.Label();
            this.DataOutput = new System.Windows.Forms.GroupBox();
            this.HandSelection = new System.Windows.Forms.Panel();
            this.RightCheck = new System.Windows.Forms.RadioButton();
            this.LeftCheck = new System.Windows.Forms.RadioButton();
            this.PrefixText = new System.Windows.Forms.TextBox();
            this.FilePrefix = new System.Windows.Forms.Label();
            this.Source1 = new System.Windows.Forms.Label();
            this.ControlInput = new System.Windows.Forms.GroupBox();
            this.Source = new System.Windows.Forms.Label();
            this.SourceSelection = new System.Windows.Forms.ComboBox();
            this.MainTabs = new System.Windows.Forms.TabControl();
            this.verticalProgressBar10 = new VerticalProgressBar();
            this.verticalProgressBar9 = new VerticalProgressBar();
            this.verticalProgressBar8 = new VerticalProgressBar();
            this.verticalProgressBar7 = new VerticalProgressBar();
            this.verticalProgressBar6 = new VerticalProgressBar();
            this.verticalProgressBar5 = new VerticalProgressBar();
            this.verticalProgressBar4 = new VerticalProgressBar();
            this.verticalProgressBar3 = new VerticalProgressBar();
            this.verticalProgressBar2 = new VerticalProgressBar();
            this.verticalProgressBar1 = new VerticalProgressBar();
            this.SummaryTab.SuspendLayout();
            this.MeasurementTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataChart)).BeginInit();
            this.fingerGroupBox.SuspendLayout();
            this.measurementGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measPictureBox)).BeginInit();
            this.CalibrationTab.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GameDisplay)).BeginInit();
            this.SettingsTab.SuspendLayout();
            this.DataSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BlocksNumeric)).BeginInit();
            this.DataOutput.SuspendLayout();
            this.HandSelection.SuspendLayout();
            this.ControlInput.SuspendLayout();
            this.MainTabs.SuspendLayout();
            this.SuspendLayout();
            // 
            // TaskTimer
            // 
            this.TaskTimer.Tick += new System.EventHandler(this.TaskTimer_Tick);
            // 
            // SummaryTab
            // 
            this.SummaryTab.Controls.Add(this.extSummary);
            this.SummaryTab.Controls.Add(this.label8);
            this.SummaryTab.Controls.Add(this.flexSummary);
            this.SummaryTab.Controls.Add(this.label6);
            this.SummaryTab.Controls.Add(this.label4);
            this.SummaryTab.Controls.Add(this.calSummary);
            this.SummaryTab.Location = new System.Drawing.Point(4, 29);
            this.SummaryTab.Name = "SummaryTab";
            this.SummaryTab.Size = new System.Drawing.Size(1170, 711);
            this.SummaryTab.TabIndex = 4;
            this.SummaryTab.Text = "Summary";
            this.SummaryTab.UseVisualStyleBackColor = true;
            // 
            // extSummary
            // 
            this.extSummary.FormattingEnabled = true;
            this.extSummary.Items.AddRange(new object[] {
            "thumb",
            "index",
            "middle",
            "ring",
            "pinky"});
            this.extSummary.Location = new System.Drawing.Point(81, 446);
            this.extSummary.Name = "extSummary";
            this.extSummary.Size = new System.Drawing.Size(291, 130);
            this.extSummary.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(85, 423);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(189, 20);
            this.label8.TabIndex = 4;
            this.label8.Text = "Extension Measurements";
            // 
            // flexSummary
            // 
            this.flexSummary.FormattingEnabled = true;
            this.flexSummary.Items.AddRange(new object[] {
            "thumb",
            "index",
            "middle",
            "ring",
            "pinky"});
            this.flexSummary.Location = new System.Drawing.Point(81, 262);
            this.flexSummary.Name = "flexSummary";
            this.flexSummary.Size = new System.Drawing.Size(291, 130);
            this.flexSummary.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(85, 238);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(169, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "Flexion Measurements";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(85, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Calibration";
            // 
            // calSummary
            // 
            this.calSummary.FormattingEnabled = true;
            this.calSummary.Items.AddRange(new object[] {
            "neutral",
            "extension",
            "flexion"});
            this.calSummary.Location = new System.Drawing.Point(81, 119);
            this.calSummary.Name = "calSummary";
            this.calSummary.Size = new System.Drawing.Size(291, 88);
            this.calSummary.TabIndex = 0;
            // 
            // MeasurementTab
            // 
            this.MeasurementTab.Controls.Add(this.dataChart);
            this.MeasurementTab.Controls.Add(this.fingerGroupBox);
            this.MeasurementTab.Controls.Add(this.measurementGroupBox);
            this.MeasurementTab.Controls.Add(this.measPictureBox);
            this.MeasurementTab.Controls.Add(this.label5);
            this.MeasurementTab.Controls.Add(this.label3);
            this.MeasurementTab.Controls.Add(this.indStartButton);
            this.MeasurementTab.Controls.Add(this.measurementTitle);
            this.MeasurementTab.Location = new System.Drawing.Point(4, 29);
            this.MeasurementTab.Name = "MeasurementTab";
            this.MeasurementTab.Size = new System.Drawing.Size(1170, 711);
            this.MeasurementTab.TabIndex = 2;
            this.MeasurementTab.Text = "Individuation";
            this.MeasurementTab.UseVisualStyleBackColor = true;
            // 
            // dataChart
            // 
            chartArea1.Name = "ChartArea1";
            this.dataChart.ChartAreas.Add(chartArea1);
            this.dataChart.Location = new System.Drawing.Point(480, 452);
            this.dataChart.Name = "dataChart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.dataChart.Series.Add(series1);
            this.dataChart.Size = new System.Drawing.Size(503, 207);
            this.dataChart.TabIndex = 14;
            this.dataChart.Text = "chart1";
            // 
            // fingerGroupBox
            // 
            this.fingerGroupBox.Controls.Add(this.FingerCheckP);
            this.fingerGroupBox.Controls.Add(this.FingerCheckR);
            this.fingerGroupBox.Controls.Add(this.FingerCheckM);
            this.fingerGroupBox.Controls.Add(this.FingerCheckI);
            this.fingerGroupBox.Controls.Add(this.FingerCheckT);
            this.fingerGroupBox.Location = new System.Drawing.Point(100, 250);
            this.fingerGroupBox.Name = "fingerGroupBox";
            this.fingerGroupBox.Size = new System.Drawing.Size(212, 197);
            this.fingerGroupBox.TabIndex = 13;
            this.fingerGroupBox.TabStop = false;
            this.fingerGroupBox.Text = "Finger";
            // 
            // FingerCheckP
            // 
            this.FingerCheckP.AutoSize = true;
            this.FingerCheckP.Location = new System.Drawing.Point(23, 150);
            this.FingerCheckP.Name = "FingerCheckP";
            this.FingerCheckP.Size = new System.Drawing.Size(71, 24);
            this.FingerCheckP.TabIndex = 4;
            this.FingerCheckP.TabStop = true;
            this.FingerCheckP.Text = "Pinky";
            this.FingerCheckP.UseVisualStyleBackColor = true;
            // 
            // FingerCheckR
            // 
            this.FingerCheckR.AutoSize = true;
            this.FingerCheckR.Location = new System.Drawing.Point(23, 120);
            this.FingerCheckR.Name = "FingerCheckR";
            this.FingerCheckR.Size = new System.Drawing.Size(67, 24);
            this.FingerCheckR.TabIndex = 3;
            this.FingerCheckR.TabStop = true;
            this.FingerCheckR.Text = "Ring";
            this.FingerCheckR.UseVisualStyleBackColor = true;
            // 
            // FingerCheckM
            // 
            this.FingerCheckM.AutoSize = true;
            this.FingerCheckM.Location = new System.Drawing.Point(23, 90);
            this.FingerCheckM.Name = "FingerCheckM";
            this.FingerCheckM.Size = new System.Drawing.Size(80, 24);
            this.FingerCheckM.TabIndex = 2;
            this.FingerCheckM.TabStop = true;
            this.FingerCheckM.Text = "Middle";
            this.FingerCheckM.UseVisualStyleBackColor = true;
            // 
            // FingerCheckI
            // 
            this.FingerCheckI.AutoSize = true;
            this.FingerCheckI.Location = new System.Drawing.Point(23, 60);
            this.FingerCheckI.Name = "FingerCheckI";
            this.FingerCheckI.Size = new System.Drawing.Size(73, 24);
            this.FingerCheckI.TabIndex = 1;
            this.FingerCheckI.TabStop = true;
            this.FingerCheckI.Text = "Index";
            this.FingerCheckI.UseVisualStyleBackColor = true;
            // 
            // FingerCheckT
            // 
            this.FingerCheckT.AutoSize = true;
            this.FingerCheckT.Location = new System.Drawing.Point(23, 30);
            this.FingerCheckT.Name = "FingerCheckT";
            this.FingerCheckT.Size = new System.Drawing.Size(83, 24);
            this.FingerCheckT.TabIndex = 0;
            this.FingerCheckT.TabStop = true;
            this.FingerCheckT.Text = "Thumb";
            this.FingerCheckT.UseVisualStyleBackColor = true;
            // 
            // measurementGroupBox
            // 
            this.measurementGroupBox.Controls.Add(this.measFlexCheck);
            this.measurementGroupBox.Controls.Add(this.measExtCheck);
            this.measurementGroupBox.Location = new System.Drawing.Point(102, 130);
            this.measurementGroupBox.Name = "measurementGroupBox";
            this.measurementGroupBox.Size = new System.Drawing.Size(200, 100);
            this.measurementGroupBox.TabIndex = 12;
            this.measurementGroupBox.TabStop = false;
            this.measurementGroupBox.Text = "Measurement";
            // 
            // measFlexCheck
            // 
            this.measFlexCheck.AutoSize = true;
            this.measFlexCheck.Location = new System.Drawing.Point(23, 60);
            this.measFlexCheck.Name = "measFlexCheck";
            this.measFlexCheck.Size = new System.Drawing.Size(84, 24);
            this.measFlexCheck.TabIndex = 1;
            this.measFlexCheck.TabStop = true;
            this.measFlexCheck.Text = "Flexion";
            this.measFlexCheck.UseVisualStyleBackColor = true;
            // 
            // measExtCheck
            // 
            this.measExtCheck.AutoSize = true;
            this.measExtCheck.Location = new System.Drawing.Point(23, 30);
            this.measExtCheck.Name = "measExtCheck";
            this.measExtCheck.Size = new System.Drawing.Size(104, 24);
            this.measExtCheck.TabIndex = 0;
            this.measExtCheck.TabStop = true;
            this.measExtCheck.Text = "Extension";
            this.measExtCheck.UseVisualStyleBackColor = true;
            // 
            // measPictureBox
            // 
            this.measPictureBox.Location = new System.Drawing.Point(480, 115);
            this.measPictureBox.Name = "measPictureBox";
            this.measPictureBox.Size = new System.Drawing.Size(503, 298);
            this.measPictureBox.TabIndex = 11;
            this.measPictureBox.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(210, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "inactive";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(100, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Glove Status:";
            // 
            // indStartButton
            // 
            this.indStartButton.Location = new System.Drawing.Point(104, 492);
            this.indStartButton.Name = "indStartButton";
            this.indStartButton.Size = new System.Drawing.Size(210, 40);
            this.indStartButton.TabIndex = 7;
            this.indStartButton.Text = "Start Measurement";
            this.indStartButton.UseVisualStyleBackColor = true;
            this.indStartButton.Click += new System.EventHandler(this.indStartButton_Click);
            // 
            // measurementTitle
            // 
            this.measurementTitle.AutoSize = true;
            this.measurementTitle.Location = new System.Drawing.Point(530, 30);
            this.measurementTitle.Name = "measurementTitle";
            this.measurementTitle.Size = new System.Drawing.Size(88, 20);
            this.measurementTitle.TabIndex = 6;
            this.measurementTitle.Text = "Participant:";
            // 
            // CalibrationTab
            // 
            this.CalibrationTab.Controls.Add(this.verticalProgressBar10);
            this.CalibrationTab.Controls.Add(this.verticalProgressBar9);
            this.CalibrationTab.Controls.Add(this.verticalProgressBar8);
            this.CalibrationTab.Controls.Add(this.verticalProgressBar7);
            this.CalibrationTab.Controls.Add(this.verticalProgressBar6);
            this.CalibrationTab.Controls.Add(this.verticalProgressBar5);
            this.CalibrationTab.Controls.Add(this.verticalProgressBar4);
            this.CalibrationTab.Controls.Add(this.verticalProgressBar3);
            this.CalibrationTab.Controls.Add(this.verticalProgressBar2);
            this.CalibrationTab.Controls.Add(this.verticalProgressBar1);
            this.CalibrationTab.Controls.Add(this.groupBox3);
            this.CalibrationTab.Controls.Add(this.label2);
            this.CalibrationTab.Controls.Add(this.label1);
            this.CalibrationTab.Controls.Add(this.calPictureBox);
            this.CalibrationTab.Controls.Add(this.calStartButton);
            this.CalibrationTab.Controls.Add(this.CalibrationTitle);
            this.CalibrationTab.Controls.Add(this.Right1);
            this.CalibrationTab.Controls.Add(this.Left1);
            this.CalibrationTab.Controls.Add(this.calGloveStatus);
            this.CalibrationTab.Controls.Add(this.calGloveStatusLabel);
            this.CalibrationTab.Controls.Add(this.GameDisplay);
            this.CalibrationTab.Location = new System.Drawing.Point(4, 29);
            this.CalibrationTab.Name = "CalibrationTab";
            this.CalibrationTab.Padding = new System.Windows.Forms.Padding(3);
            this.CalibrationTab.Size = new System.Drawing.Size(1170, 711);
            this.CalibrationTab.TabIndex = 1;
            this.CalibrationTab.Text = "Calibration";
            this.CalibrationTab.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.extRadioB);
            this.groupBox3.Controls.Add(this.flexRadioB);
            this.groupBox3.Controls.Add(this.neutralRadioB);
            this.groupBox3.Location = new System.Drawing.Point(99, 144);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(210, 165);
            this.groupBox3.TabIndex = 38;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Calibration Type";
            // 
            // extRadioB
            // 
            this.extRadioB.AutoSize = true;
            this.extRadioB.Location = new System.Drawing.Point(28, 79);
            this.extRadioB.Name = "extRadioB";
            this.extRadioB.Size = new System.Drawing.Size(104, 24);
            this.extRadioB.TabIndex = 3;
            this.extRadioB.TabStop = true;
            this.extRadioB.Text = "Extension";
            this.extRadioB.UseVisualStyleBackColor = true;
            // 
            // flexRadioB
            // 
            this.flexRadioB.AutoSize = true;
            this.flexRadioB.Location = new System.Drawing.Point(28, 121);
            this.flexRadioB.Name = "flexRadioB";
            this.flexRadioB.Size = new System.Drawing.Size(84, 24);
            this.flexRadioB.TabIndex = 2;
            this.flexRadioB.TabStop = true;
            this.flexRadioB.Text = "Flexion";
            this.flexRadioB.UseVisualStyleBackColor = true;
            // 
            // neutralRadioB
            // 
            this.neutralRadioB.AutoSize = true;
            this.neutralRadioB.Location = new System.Drawing.Point(28, 40);
            this.neutralRadioB.Name = "neutralRadioB";
            this.neutralRadioB.Size = new System.Drawing.Size(145, 24);
            this.neutralRadioB.TabIndex = 1;
            this.neutralRadioB.TabStop = true;
            this.neutralRadioB.Text = "Neutral Position";
            this.neutralRadioB.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(440, 599);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 20);
            this.label2.TabIndex = 36;
            this.label2.Text = "_";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(440, 514);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 20);
            this.label1.TabIndex = 35;
            this.label1.Text = "_";
            // 
            // calPictureBox
            // 
            this.calPictureBox.Location = new System.Drawing.Point(480, 115);
            this.calPictureBox.Name = "calPictureBox";
            this.calPictureBox.Size = new System.Drawing.Size(503, 298);
            this.calPictureBox.TabIndex = 20;
            this.calPictureBox.TabStop = false;
            // 
            // calStartButton
            // 
            this.calStartButton.Location = new System.Drawing.Point(99, 428);
            this.calStartButton.Name = "calStartButton";
            this.calStartButton.Size = new System.Drawing.Size(210, 40);
            this.calStartButton.TabIndex = 19;
            this.calStartButton.Text = "Start Calibration";
            this.calStartButton.UseVisualStyleBackColor = true;
            this.calStartButton.Click += new System.EventHandler(this.calStartButton_Click);
            // 
            // CalibrationTitle
            // 
            this.CalibrationTitle.AutoSize = true;
            this.CalibrationTitle.Location = new System.Drawing.Point(537, 31);
            this.CalibrationTitle.Name = "CalibrationTitle";
            this.CalibrationTitle.Size = new System.Drawing.Size(88, 20);
            this.CalibrationTitle.TabIndex = 5;
            this.CalibrationTitle.Text = "Participant:";
            // 
            // Right1
            // 
            this.Right1.AutoSize = true;
            this.Right1.Location = new System.Drawing.Point(835, 80);
            this.Right1.Name = "Right1";
            this.Right1.Size = new System.Drawing.Size(47, 20);
            this.Right1.TabIndex = 4;
            this.Right1.Text = "Right";
            // 
            // Left1
            // 
            this.Left1.AutoSize = true;
            this.Left1.Location = new System.Drawing.Point(573, 80);
            this.Left1.Name = "Left1";
            this.Left1.Size = new System.Drawing.Size(37, 20);
            this.Left1.TabIndex = 3;
            this.Left1.Text = "Left";
            // 
            // calGloveStatus
            // 
            this.calGloveStatus.AutoSize = true;
            this.calGloveStatus.Location = new System.Drawing.Point(210, 80);
            this.calGloveStatus.Name = "calGloveStatus";
            this.calGloveStatus.Size = new System.Drawing.Size(62, 20);
            this.calGloveStatus.TabIndex = 2;
            this.calGloveStatus.Text = "inactive";
            // 
            // calGloveStatusLabel
            // 
            this.calGloveStatusLabel.AutoSize = true;
            this.calGloveStatusLabel.Location = new System.Drawing.Point(100, 80);
            this.calGloveStatusLabel.Name = "calGloveStatusLabel";
            this.calGloveStatusLabel.Size = new System.Drawing.Size(105, 20);
            this.calGloveStatusLabel.TabIndex = 1;
            this.calGloveStatusLabel.Text = "Glove Status:";
            // 
            // GameDisplay
            // 
            this.GameDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GameDisplay.Location = new System.Drawing.Point(3, 3);
            this.GameDisplay.Name = "GameDisplay";
            this.GameDisplay.Size = new System.Drawing.Size(1164, 705);
            this.GameDisplay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.GameDisplay.TabIndex = 0;
            this.GameDisplay.TabStop = false;
            // 
            // SettingsTab
            // 
            this.SettingsTab.Controls.Add(this.DataSettings);
            this.SettingsTab.Controls.Add(this.DataOutput);
            this.SettingsTab.Controls.Add(this.ControlInput);
            this.SettingsTab.Location = new System.Drawing.Point(4, 29);
            this.SettingsTab.Name = "SettingsTab";
            this.SettingsTab.Padding = new System.Windows.Forms.Padding(3);
            this.SettingsTab.Size = new System.Drawing.Size(1170, 711);
            this.SettingsTab.TabIndex = 0;
            this.SettingsTab.Text = "Settings";
            this.SettingsTab.UseVisualStyleBackColor = true;
            // 
            // DataSettings
            // 
            this.DataSettings.Controls.Add(this.TimeoutUnits);
            this.DataSettings.Controls.Add(this.BasicSounds);
            this.DataSettings.Controls.Add(this.AudioOn);
            this.DataSettings.Controls.Add(this.BlocksNumeric);
            this.DataSettings.Controls.Add(this.TimeoutLabel);
            this.DataSettings.Location = new System.Drawing.Point(19, 358);
            this.DataSettings.Name = "DataSettings";
            this.DataSettings.Size = new System.Drawing.Size(393, 143);
            this.DataSettings.TabIndex = 2;
            this.DataSettings.TabStop = false;
            this.DataSettings.Text = "Acquisition Settings";
            // 
            // TimeoutUnits
            // 
            this.TimeoutUnits.AutoSize = true;
            this.TimeoutUnits.Location = new System.Drawing.Point(188, 43);
            this.TimeoutUnits.Name = "TimeoutUnits";
            this.TimeoutUnits.Size = new System.Drawing.Size(69, 20);
            this.TimeoutUnits.TabIndex = 6;
            this.TimeoutUnits.Text = "seconds";
            // 
            // BasicSounds
            // 
            this.BasicSounds.AutoSize = true;
            this.BasicSounds.Location = new System.Drawing.Point(107, 93);
            this.BasicSounds.Name = "BasicSounds";
            this.BasicSounds.Size = new System.Drawing.Size(166, 24);
            this.BasicSounds.TabIndex = 5;
            this.BasicSounds.TabStop = true;
            this.BasicSounds.Text = "Start/Stop Sounds";
            this.BasicSounds.UseVisualStyleBackColor = true;
            this.BasicSounds.CheckedChanged += new System.EventHandler(this.CompletionSounds_CheckedChanged);
            // 
            // AudioOn
            // 
            this.AudioOn.AutoSize = true;
            this.AudioOn.Location = new System.Drawing.Point(38, 95);
            this.AudioOn.Name = "AudioOn";
            this.AudioOn.Size = new System.Drawing.Size(54, 20);
            this.AudioOn.TabIndex = 4;
            this.AudioOn.Text = "Audio:";
            // 
            // BlocksNumeric
            // 
            this.BlocksNumeric.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.BlocksNumeric.Location = new System.Drawing.Point(107, 41);
            this.BlocksNumeric.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.BlocksNumeric.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.BlocksNumeric.Name = "BlocksNumeric";
            this.BlocksNumeric.Size = new System.Drawing.Size(75, 26);
            this.BlocksNumeric.TabIndex = 2;
            this.BlocksNumeric.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.BlocksNumeric.ValueChanged += new System.EventHandler(this.BlocksNumeric_ValueChanged);
            // 
            // TimeoutLabel
            // 
            this.TimeoutLabel.AutoSize = true;
            this.TimeoutLabel.Location = new System.Drawing.Point(32, 41);
            this.TimeoutLabel.Name = "TimeoutLabel";
            this.TimeoutLabel.Size = new System.Drawing.Size(66, 20);
            this.TimeoutLabel.TabIndex = 0;
            this.TimeoutLabel.Text = "Timeout";
            // 
            // DataOutput
            // 
            this.DataOutput.Controls.Add(this.HandSelection);
            this.DataOutput.Controls.Add(this.PrefixText);
            this.DataOutput.Controls.Add(this.FilePrefix);
            this.DataOutput.Controls.Add(this.Source1);
            this.DataOutput.Location = new System.Drawing.Point(19, 195);
            this.DataOutput.Name = "DataOutput";
            this.DataOutput.Size = new System.Drawing.Size(393, 142);
            this.DataOutput.TabIndex = 1;
            this.DataOutput.TabStop = false;
            this.DataOutput.Text = "Data Output";
            // 
            // HandSelection
            // 
            this.HandSelection.Controls.Add(this.RightCheck);
            this.HandSelection.Controls.Add(this.LeftCheck);
            this.HandSelection.Location = new System.Drawing.Point(139, 80);
            this.HandSelection.Name = "HandSelection";
            this.HandSelection.Size = new System.Drawing.Size(207, 35);
            this.HandSelection.TabIndex = 4;
            // 
            // RightCheck
            // 
            this.RightCheck.AutoSize = true;
            this.RightCheck.Location = new System.Drawing.Point(116, 3);
            this.RightCheck.Name = "RightCheck";
            this.RightCheck.Size = new System.Drawing.Size(72, 24);
            this.RightCheck.TabIndex = 1;
            this.RightCheck.TabStop = true;
            this.RightCheck.Text = "Right";
            this.RightCheck.UseVisualStyleBackColor = true;
            this.RightCheck.Click += new System.EventHandler(this.RightCheck_OnClick);
            // 
            // LeftCheck
            // 
            this.LeftCheck.AutoSize = true;
            this.LeftCheck.Location = new System.Drawing.Point(3, 3);
            this.LeftCheck.Name = "LeftCheck";
            this.LeftCheck.Size = new System.Drawing.Size(62, 24);
            this.LeftCheck.TabIndex = 0;
            this.LeftCheck.TabStop = true;
            this.LeftCheck.Text = "Left";
            this.LeftCheck.UseVisualStyleBackColor = true;
            this.LeftCheck.Click += new System.EventHandler(this.LeftCheck_OnClick);
            // 
            // PrefixText
            // 
            this.PrefixText.Location = new System.Drawing.Point(139, 35);
            this.PrefixText.Name = "PrefixText";
            this.PrefixText.Size = new System.Drawing.Size(207, 26);
            this.PrefixText.TabIndex = 3;
            this.PrefixText.TextChanged += new System.EventHandler(this.PrefixText_TextChanged);
            // 
            // FilePrefix
            // 
            this.FilePrefix.AutoSize = true;
            this.FilePrefix.Location = new System.Drawing.Point(18, 35);
            this.FilePrefix.Name = "FilePrefix";
            this.FilePrefix.Size = new System.Drawing.Size(109, 20);
            this.FilePrefix.TabIndex = 2;
            this.FilePrefix.Text = "Participant ID:";
            // 
            // Source1
            // 
            this.Source1.AutoSize = true;
            this.Source1.Location = new System.Drawing.Point(75, 80);
            this.Source1.Name = "Source1";
            this.Source1.Size = new System.Drawing.Size(52, 20);
            this.Source1.TabIndex = 0;
            this.Source1.Text = "Hand:";
            // 
            // ControlInput
            // 
            this.ControlInput.Controls.Add(this.Source);
            this.ControlInput.Controls.Add(this.SourceSelection);
            this.ControlInput.Location = new System.Drawing.Point(19, 70);
            this.ControlInput.Name = "ControlInput";
            this.ControlInput.Size = new System.Drawing.Size(393, 105);
            this.ControlInput.TabIndex = 0;
            this.ControlInput.TabStop = false;
            this.ControlInput.Text = " Control Input";
            // 
            // Source
            // 
            this.Source.AutoSize = true;
            this.Source.Location = new System.Drawing.Point(63, 35);
            this.Source.Name = "Source";
            this.Source.Size = new System.Drawing.Size(64, 20);
            this.Source.TabIndex = 1;
            this.Source.Text = "Source:";
            // 
            // SourceSelection
            // 
            this.SourceSelection.FormattingEnabled = true;
            this.SourceSelection.Location = new System.Drawing.Point(139, 35);
            this.SourceSelection.Name = "SourceSelection";
            this.SourceSelection.Size = new System.Drawing.Size(207, 28);
            this.SourceSelection.TabIndex = 0;
            this.SourceSelection.SelectedIndexChanged += new System.EventHandler(this.SourceSelection_SelectedIndexChanged);
            this.SourceSelection.Click += new System.EventHandler(this.SourceSelection_OnClick);
            // 
            // MainTabs
            // 
            this.MainTabs.Controls.Add(this.SettingsTab);
            this.MainTabs.Controls.Add(this.CalibrationTab);
            this.MainTabs.Controls.Add(this.MeasurementTab);
            this.MainTabs.Controls.Add(this.SummaryTab);
            this.MainTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTabs.Location = new System.Drawing.Point(0, 0);
            this.MainTabs.Name = "MainTabs";
            this.MainTabs.SelectedIndex = 0;
            this.MainTabs.Size = new System.Drawing.Size(1178, 744);
            this.MainTabs.TabIndex = 0;
            // 
            // verticalProgressBar10
            // 
            this.verticalProgressBar10.Location = new System.Drawing.Point(981, 520);
            this.verticalProgressBar10.Maximum = 255;
            this.verticalProgressBar10.Name = "verticalProgressBar10";
            this.verticalProgressBar10.Size = new System.Drawing.Size(30, 100);
            this.verticalProgressBar10.Step = 1;
            this.verticalProgressBar10.TabIndex = 34;
            // 
            // verticalProgressBar9
            // 
            this.verticalProgressBar9.Location = new System.Drawing.Point(935, 480);
            this.verticalProgressBar9.Maximum = 255;
            this.verticalProgressBar9.Name = "verticalProgressBar9";
            this.verticalProgressBar9.Size = new System.Drawing.Size(25, 140);
            this.verticalProgressBar9.Step = 1;
            this.verticalProgressBar9.TabIndex = 33;
            // 
            // verticalProgressBar8
            // 
            this.verticalProgressBar8.Location = new System.Drawing.Point(890, 480);
            this.verticalProgressBar8.Maximum = 255;
            this.verticalProgressBar8.Name = "verticalProgressBar8";
            this.verticalProgressBar8.Size = new System.Drawing.Size(25, 140);
            this.verticalProgressBar8.Step = 1;
            this.verticalProgressBar8.TabIndex = 32;
            // 
            // verticalProgressBar7
            // 
            this.verticalProgressBar7.Location = new System.Drawing.Point(843, 480);
            this.verticalProgressBar7.Maximum = 255;
            this.verticalProgressBar7.Name = "verticalProgressBar7";
            this.verticalProgressBar7.Size = new System.Drawing.Size(25, 140);
            this.verticalProgressBar7.Step = 1;
            this.verticalProgressBar7.TabIndex = 31;
            // 
            // verticalProgressBar6
            // 
            this.verticalProgressBar6.Location = new System.Drawing.Point(800, 510);
            this.verticalProgressBar6.Maximum = 255;
            this.verticalProgressBar6.Name = "verticalProgressBar6";
            this.verticalProgressBar6.Size = new System.Drawing.Size(25, 110);
            this.verticalProgressBar6.Step = 1;
            this.verticalProgressBar6.TabIndex = 30;
            // 
            // verticalProgressBar5
            // 
            this.verticalProgressBar5.Location = new System.Drawing.Point(645, 510);
            this.verticalProgressBar5.Maximum = 255;
            this.verticalProgressBar5.Name = "verticalProgressBar5";
            this.verticalProgressBar5.Size = new System.Drawing.Size(25, 110);
            this.verticalProgressBar5.Step = 1;
            this.verticalProgressBar5.TabIndex = 29;
            // 
            // verticalProgressBar4
            // 
            this.verticalProgressBar4.Location = new System.Drawing.Point(600, 480);
            this.verticalProgressBar4.Maximum = 255;
            this.verticalProgressBar4.Name = "verticalProgressBar4";
            this.verticalProgressBar4.Size = new System.Drawing.Size(25, 140);
            this.verticalProgressBar4.Step = 1;
            this.verticalProgressBar4.TabIndex = 28;
            // 
            // verticalProgressBar3
            // 
            this.verticalProgressBar3.Location = new System.Drawing.Point(555, 480);
            this.verticalProgressBar3.Maximum = 255;
            this.verticalProgressBar3.Name = "verticalProgressBar3";
            this.verticalProgressBar3.Size = new System.Drawing.Size(25, 140);
            this.verticalProgressBar3.Step = 1;
            this.verticalProgressBar3.TabIndex = 27;
            // 
            // verticalProgressBar2
            // 
            this.verticalProgressBar2.Location = new System.Drawing.Point(510, 480);
            this.verticalProgressBar2.Maximum = 255;
            this.verticalProgressBar2.Name = "verticalProgressBar2";
            this.verticalProgressBar2.Size = new System.Drawing.Size(25, 140);
            this.verticalProgressBar2.Step = 1;
            this.verticalProgressBar2.TabIndex = 26;
            // 
            // verticalProgressBar1
            // 
            this.verticalProgressBar1.Location = new System.Drawing.Point(460, 520);
            this.verticalProgressBar1.Maximum = 255;
            this.verticalProgressBar1.Name = "verticalProgressBar1";
            this.verticalProgressBar1.Size = new System.Drawing.Size(30, 100);
            this.verticalProgressBar1.Step = 1;
            this.verticalProgressBar1.TabIndex = 25;
            // 
            // TargetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 744);
            this.Controls.Add(this.MainTabs);
            this.Name = "TargetForm";
            this.Text = "Individuation Assessment";
            this.SummaryTab.ResumeLayout(false);
            this.SummaryTab.PerformLayout();
            this.MeasurementTab.ResumeLayout(false);
            this.MeasurementTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataChart)).EndInit();
            this.fingerGroupBox.ResumeLayout(false);
            this.fingerGroupBox.PerformLayout();
            this.measurementGroupBox.ResumeLayout(false);
            this.measurementGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measPictureBox)).EndInit();
            this.CalibrationTab.ResumeLayout(false);
            this.CalibrationTab.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GameDisplay)).EndInit();
            this.SettingsTab.ResumeLayout(false);
            this.DataSettings.ResumeLayout(false);
            this.DataSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BlocksNumeric)).EndInit();
            this.DataOutput.ResumeLayout(false);
            this.DataOutput.PerformLayout();
            this.HandSelection.ResumeLayout(false);
            this.HandSelection.PerformLayout();
            this.ControlInput.ResumeLayout(false);
            this.ControlInput.PerformLayout();
            this.MainTabs.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.IO.Ports.SerialPort MySerialPort;
        private System.Windows.Forms.Timer TaskTimer;
        private System.Windows.Forms.TabPage SummaryTab;
        private System.Windows.Forms.TabPage MeasurementTab;
        private System.Windows.Forms.GroupBox fingerGroupBox;
        private System.Windows.Forms.GroupBox measurementGroupBox;
        private System.Windows.Forms.RadioButton measFlexCheck;
        private System.Windows.Forms.RadioButton measExtCheck;
        private System.Windows.Forms.PictureBox measPictureBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button indStartButton;
        private System.Windows.Forms.Label measurementTitle;
        private System.Windows.Forms.TabPage CalibrationTab;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox calPictureBox;
        private System.Windows.Forms.Button calStartButton;
        private System.Windows.Forms.Label CalibrationTitle;
        private System.Windows.Forms.Label Right1;
        private System.Windows.Forms.Label Left1;
        private System.Windows.Forms.Label calGloveStatus;
        private System.Windows.Forms.Label calGloveStatusLabel;
        private System.Windows.Forms.PictureBox GameDisplay;
        private System.Windows.Forms.TabPage SettingsTab;
        private System.Windows.Forms.GroupBox DataSettings;
        private System.Windows.Forms.Label TimeoutUnits;
        private System.Windows.Forms.RadioButton BasicSounds;
        private System.Windows.Forms.Label AudioOn;
        private System.Windows.Forms.NumericUpDown BlocksNumeric;
        private System.Windows.Forms.Label TimeoutLabel;
        private System.Windows.Forms.GroupBox DataOutput;
        private System.Windows.Forms.Panel HandSelection;
        private System.Windows.Forms.RadioButton RightCheck;
        private System.Windows.Forms.RadioButton LeftCheck;
        private System.Windows.Forms.TextBox PrefixText;
        private System.Windows.Forms.Label FilePrefix;
        private System.Windows.Forms.Label Source1;
        private System.Windows.Forms.GroupBox ControlInput;
        private System.Windows.Forms.Label Source;
        private System.Windows.Forms.ComboBox SourceSelection;
        private System.Windows.Forms.TabControl MainTabs;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton extRadioB;
        private System.Windows.Forms.RadioButton flexRadioB;
        private System.Windows.Forms.RadioButton neutralRadioB;
        private System.Windows.Forms.RadioButton FingerCheckP;
        private System.Windows.Forms.RadioButton FingerCheckR;
        private System.Windows.Forms.RadioButton FingerCheckM;
        private System.Windows.Forms.RadioButton FingerCheckI;
        private System.Windows.Forms.RadioButton FingerCheckT;
        private System.Windows.Forms.CheckedListBox extSummary;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckedListBox flexSummary;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckedListBox calSummary;
        private VerticalProgressBar verticalProgressBar10;
        private VerticalProgressBar verticalProgressBar9;
        private VerticalProgressBar verticalProgressBar8;
        private VerticalProgressBar verticalProgressBar7;
        private VerticalProgressBar verticalProgressBar6;
        private VerticalProgressBar verticalProgressBar5;
        private VerticalProgressBar verticalProgressBar4;
        private VerticalProgressBar verticalProgressBar3;
        private VerticalProgressBar verticalProgressBar2;
        private VerticalProgressBar verticalProgressBar1;
        private System.Windows.Forms.DataVisualization.Charting.Chart dataChart;
    }
}

