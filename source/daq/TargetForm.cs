﻿/*
(c) 2003 AD Gomez

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// serial ports
using System.IO.Ports;

// sounds
using System.Media;

// csv
using CsvHelper;
using CsvHelper.Configuration;


namespace Target
{
    public partial class TargetForm : Form
    {
        // *** broad-scope vars


        // file save

        string dataPrefix = "DOE";

        string dataPath;

        string dataFile;


        // memory save

        int dataStateCount = 0;

        int dataSampleInterval = 100;  // 100 ms = 10 S/s, 50 ms = 20 S/s, 25 ms = 40 S/s, 10 ms = 100 S/s  
        int dataMaxStates = 1000;


        bool UpdatingData = false;

        bool SavingData = false;
        
        List<GloveState> gloveStateList = new List<GloveState>();


        // glove data
        const int rawStateLen = 21;
        byte[] rawState = new byte[rawStateLen];

        // hand
        Hands handedness;

        // finger
        Fingers fingerness;

        // calibration
        CalType calibration;
        MeasType measurement;

        // *** main methods

        // main form
        public TargetForm()
        {
            // load method
            this.Load += new EventHandler(TargetForm_load);

            // key press method
            this.KeyPress += new KeyPressEventHandler(StartPause);

            // all other components
            InitializeComponent();

            // adding elements
            MainTabs.SelectedIndexChanged += new EventHandler(MainTabs_SelectedIndexChanged);
            MainTabs.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(MainTabs_Selecting);

            this.neutralRadioB.Click += new System.EventHandler(this.neutralRadioB_OnClick);
            this.flexRadioB.Click += new System.EventHandler(this.flexRadioB_OnClick);
            this.extRadioB.Click += new System.EventHandler(this.extRadioB_OnClick);

            this.FingerCheckT.Click += new System.EventHandler(this.fingerCheckT_OnClick);
            this.FingerCheckI.Click += new System.EventHandler(this.fingerCheckI_OnClick);
            this.FingerCheckM.Click += new System.EventHandler(this.fingerCheckM_OnClick);
            this.FingerCheckR.Click += new System.EventHandler(this.fingerCheckR_OnClick);
            this.FingerCheckP.Click += new System.EventHandler(this.fingerCheckP_OnClick);

            this.measExtCheck.Click += new System.EventHandler(this.measExtCheck_OnClick);
            this.measFlexCheck.Click += new System.EventHandler(this.measFlexCheck_OnClick);
           
        }





        // general initialization
        private void TargetForm_load(object sender, System.EventArgs e)
        {

            Console.WriteLine("Starting Individuation App ... ");

            // *** initialize ports 

            UpdatePorts();
            SourceSelection.SelectedIndex = 0;

            // *** initialize data saving

            // default data prefix
            PrefixText.Text = dataPrefix;


            // *** procedure

            // hand selection
            LeftCheck.Checked = true;
            handedness = (Hands)HandSelection.Controls.OfType<RadioButton>().FirstOrDefault(n => n.Checked).TabIndex;

            Left1.Enabled = LeftCheck.Checked;
            Right1.Enabled = RightCheck.Checked;

            // calibration
            neutralRadioB.Checked = true;
            calibration = CalType.neutral;
            calPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + calibration.ToString() + ".png";
            calPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            CalibrationTitle.Text = "Participant: " + dataPrefix + " (Hold pose then press 'Start')";

            // measurement
            measFlexCheck.Checked = true;
            measurement = MeasType.flexion;
            measPictureBox.ImageLocation = measPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + measurement.ToString() + fingerness.ToString() + ".png";
            measPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            measurementTitle.Text = "Participant: " + dataPrefix + " (Press 'Start' then move target finger)";

            // finger selection
            FingerCheckT.Checked = true;
            fingerness = (Fingers)fingerGroupBox.Controls.OfType<RadioButton>().FirstOrDefault(n => n.Checked).TabIndex;

            // timeout
            BlocksNumeric.Value = (decimal)(dataMaxStates / dataSampleInterval);

        }

        // *** game flow

        // TBA: begin trial begin practice

        private void StartPause(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 'S')
            {

            }
        }

        private void SaveSettingsButton_Click(object sender, EventArgs e)
        {
            
            // *** initialize game
 
            MainTabs.SelectedIndex = 1;


        }


        private void MainTabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (MainTabs.SelectedTab == MainTabs.TabPages[0])
            {
                StopDataUpdates();
            }
            else if (MainTabs.SelectedTab == MainTabs.TabPages[3])
            {
                StopDataUpdates();
            }
            else
            {

                if (!UpdatingData)
                {
                    dataPath = "./" + dataPrefix;
                    System.IO.Directory.CreateDirectory(dataPath);
                    StartDataUpdates();
                }                
            }
        }

        private void MainTabs_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (e.TabPageIndex < 0) return;
            e.Cancel = !e.TabPage.Enabled;
        }


        private void calStartButton_Click(object sender, EventArgs e)
        {
            if (!SavingData)
            {
                StartDataSave();

                if (SavingData)
                {
                    calStartButton.Text = "Stop Calibration";

                    groupBox3.Enabled = false;

                    dataFile = dataPrefix + "_" + handedness.ToString() + "_cal_" + calibration.ToString() + DateTime.Now.ToString("_yyyyMMdd_HHmmss");

                    calSummary.TabIndex = (int)calibration;


                    // this is just in case
                    foreach (TabPage tab in MainTabs.TabPages)
                    {
                        tab.Enabled = false;
                    }

                    (MainTabs.TabPages[MainTabs.SelectedIndex] as TabPage).Enabled = true;
                    calStartButton.Enabled = true;
                }
            }
            else
            {
                calSummary.SetItemChecked(calSummary.TabIndex, true);
                calSummary.Items[calSummary.TabIndex] = calibration.ToString() + " (" + handedness.ToString() + ")";

                StopDataSave();

                Reset();

            }
        }

        private void indStartButton_Click(object sender, EventArgs e)
        {
           //Console.WriteLine("measurement start");

            if (!SavingData)
            {
                StartDataSave();

                if (SavingData)
                {
                    indStartButton.Text = "Stop Measurement";

                    fingerGroupBox.Enabled = false;
                    measurementGroupBox.Enabled = false;

                    dataFile = dataPrefix + "_" + handedness.ToString() + fingerness.ToString() + "_meas_" + measurement.ToString() + DateTime.Now.ToString("_yyyyMMdd_HHmmss");

                    flexSummary.TabIndex = (int)fingerness;
                    extSummary.TabIndex = (int)fingerness;

                    // this is just in case
                    foreach (TabPage tab in MainTabs.TabPages)
                    {
                        tab.Enabled = false;
                    }

                    (MainTabs.TabPages[MainTabs.SelectedIndex] as TabPage).Enabled = true;
                    indStartButton.Enabled = true;

                }
            }
            else
            {
                FingerNames curFingerName = (FingerNames)fingerness;

                if (measurement == MeasType.extension)
                {
                    extSummary.SetItemChecked(extSummary.TabIndex, true);
                    extSummary.Items[extSummary.TabIndex] = curFingerName.ToString() + " (" + handedness.ToString() + ")";
                }

                if (measurement == MeasType.flexion)
                {
                    flexSummary.SetItemChecked(flexSummary.TabIndex, true);
                    flexSummary.Items[flexSummary.TabIndex] = curFingerName.ToString() + " (" + handedness.ToString() + ")";
                }

                StopDataSave();



                // data chart
                dataChart.Series.Clear();

                // Data arrays
                string[] seriesArray = { "T", "I", "M", "R", "P" };


                // Add series
                System.Windows.Forms.DataVisualization.Charting.Series seriesT = dataChart.Series.Add("T");
                System.Windows.Forms.DataVisualization.Charting.Series seriesI = dataChart.Series.Add("I");
                System.Windows.Forms.DataVisualization.Charting.Series seriesM = dataChart.Series.Add("M");
                System.Windows.Forms.DataVisualization.Charting.Series seriesR = dataChart.Series.Add("R");
                System.Windows.Forms.DataVisualization.Charting.Series seriesP = dataChart.Series.Add("P");

                for (int i = 0; i < gloveStateList.Count; i++)
                {
                    seriesT.Points.Add(gloveStateList[i].Tm);
                    seriesI.Points.Add(gloveStateList[i].Im);
                    seriesM.Points.Add(gloveStateList[i].Mm);
                    seriesR.Points.Add(gloveStateList[i].Rm);
                    seriesP.Points.Add(gloveStateList[i].Pm);
                }

                dataChart.Series[0].Color = Color.Blue;
                dataChart.Series[1].Color = Color.Orange;
                dataChart.Series[2].Color = Color.Red;
                dataChart.Series[3].Color = Color.Green;
                dataChart.Series[4].Color = Color.Violet;

                dataChart.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
                dataChart.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
                dataChart.Series[2].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
                dataChart.Series[3].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
                dataChart.Series[4].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;

                dataChart.Legends.Clear();
                dataChart.Legends.Add(new System.Windows.Forms.DataVisualization.Charting.Legend("Default") { Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Right });


                Reset();

            }
        }





        private void calExtStartButton_Click(object sender, EventArgs e)
        {
           // if((MainTabs.TabPages[MainTabs.SelectedIndex] as TabPage).Enabled)
            //    Console.WriteLine("DOING STUFF");

        }

        private void Reset()
        {
            groupBox3.Enabled = true;
            fingerGroupBox.Enabled = true;
            measurementGroupBox.Enabled = true;

            foreach (TabPage tab in MainTabs.TabPages)
            {
                tab.Enabled = true;
            }

            //foreach (Control cnt in MainTabs.TabPages[MainTabs.SelectedIndex])
            //{
            //}

            calStartButton.Text = "Start Calibration";
            indStartButton.Text = "Start Measurement";
        }



        private void StartDataSave()
        {
            gloveStateList.Clear();
            dataStateCount = 0;

            if(!SavingData)
                SavingData = true;


          /*  if (UpdatingData && !SavingData)
            {
                SavingData = true;
                if (BasicSounds.Checked)
                    PlayStartSound();
            }
            else
            {
                MessageBox.Show("Data not updating: Did you select a cyberglove port?");
                PlayErrorSound();
            } */
        }

        private void StopDataSave()
        {
            if (BasicSounds.Checked)
                PlayStopSound();

            if (SavingData)
            {
                using (var writer = new System.IO.StreamWriter(dataPath + "\\" + dataFile + ".csv"))
                using (var csv = new CsvWriter(writer, System.Globalization.CultureInfo.InvariantCulture))
                {
                    csv.WriteRecords(gloveStateList);
                }

                SavingData = false;
            }
        }

        private void StartDataUpdates()
        {
            Console.WriteLine("StartDataUpdates()");
            TaskTimer.Interval = dataSampleInterval;
            TaskTimer.Start();
            UpdatingData = MySerialPort.IsOpen;
        }

        private void StopDataUpdates()
        {
            Console.WriteLine("StopDataUpdates()");
            TaskTimer.Stop();
            UpdatingData = false;
        }


       private void TaskTimer_Tick(object sender, EventArgs e)
        {

            // TODO: other inputs
            // TODO: json files
            // TODO: data acqusition -- gloves
            // TODO: add timer at the end

            // *** write data
            
            // update visuals
            UpdateCalibrationVisuals();


            if (SavingData)
            {
                // Console.WriteLine("Saving");

                GloveState curState = new GloveState(rawState);

                gloveStateList.Add(curState);

                dataStateCount++;

                if(dataMaxStates < dataStateCount)
                {
                    Console.WriteLine("Timed out");

                    // reset buttons
                    Reset();

                    StopDataSave();
                }

            }
            

        }


        // plays sound
        private void PlayStartSound()
        {
            SoundPlayer sound = new SoundPlayer(@"c:\Windows\Media\Windows Ding.wav");
            sound.Play();
        }

        // plays sound
        private void PlayStopSound()
        {
            SoundPlayer sound = new SoundPlayer(@"c:\Windows\Media\tada.wav");
            sound.Play();
        }

        // plays sound
        private void PlayErrorSound()
        {
            SoundPlayer sound = new SoundPlayer(@"c:\Windows\Media\Windows Balloon.wav");
            sound.Play();
        }


        // *** settings changes

        private void UpdatePorts()
        {
            SourceSelection.Items.Clear();
            SourceSelection.Items.Add("<none>");

            Console.WriteLine("Looking for serial ports ... ");
            String[] ports = SerialPort.GetPortNames();
            SourceSelection.Items.AddRange(ports);

            foreach (var portName in ports)
                Console.WriteLine("> " + portName.ToString());
        }

        private void ChangeToPort(int portNumber)
        {
            if (MySerialPort.IsOpen)
                MySerialPort.Close();
            
            String[] ports = SerialPort.GetPortNames();

            if ((1 <= ports.Length) && (0 < portNumber))
            {
                if (portNumber <= ports.Length)
                {
                    // default speed
                    MySerialPort.BaudRate = 115200;

                    // buffer
                    MySerialPort.ReceivedBytesThreshold = rawStateLen;

                    // add serial port event if needed
                    MySerialPort.DataReceived += new SerialDataReceivedEventHandler(PortReceived);

                    // match ports
                    MySerialPort.PortName = ports[portNumber - 1];

                    // open communication
                    MySerialPort.Open();

                    // console update
                    Console.WriteLine("Connected to serial port: " + MySerialPort.PortName);

                    // label
                    calGloveStatus.Text = "Connected";
                    calGloveStatus.ForeColor = Color.Black;
                }
                else
                {
                    Console.WriteLine("Serial port unavailable");
                }
            }
            else
            {
                Console.WriteLine("No serial connection");
            }


            if (MySerialPort.IsOpen)
            {
                calGloveStatus.Text = "Connected";
                calGloveStatus.ForeColor = Color.Green;

                label5.Text = "Connected";
                label5.ForeColor = Color.Green;
            }
            else
            {
                calGloveStatus.Text = "Disconnected";
                calGloveStatus.ForeColor = Color.Red;

                label5.Text = "Disconnected";
                label5.ForeColor = Color.Red;
            }

       }


        private void SourceSelection_OnClick(object sender, EventArgs e)
        {
            UpdatePorts();
        }

        private void SourceSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeToPort(SourceSelection.SelectedIndex);
        }



        // serial port data 
        private void PortReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;

            // get data
            byte[] data = new byte[rawStateLen];
            uint offset = 0;
            sp.Read(data, 0, rawStateLen);

            // lock for end of package
            for (uint i = rawStateLen; i-- >1; )
            {
                if ( (data[i] == (byte)83) && (data[i - 1] == (byte)0))
                {
                    offset = rawStateLen - i - 1;
                    break; 
                }
            }

            // register raw values
            for (uint i = 0; i<rawStateLen; i++)
            {
                rawState[(i + offset) % rawStateLen] = data[i];
            }

           /*// show data
           foreach (var dof in rawState)
                Console.Write(dof.ToString() + ",");
           Console.WriteLine("");*/

        }

  
        // visualization
        public void UpdateCalibrationVisuals()
        {
            
            // *** handy vars
            if (rawStateLen <= rawState.Length)
            {
                if (handedness == Hands.L)
                {
                    // left
                    verticalProgressBar1.Value = (int)rawState[2];
                    verticalProgressBar2.Value = (int)rawState[5];
                    verticalProgressBar3.Value = (int)rawState[7];
                    verticalProgressBar4.Value = (int)rawState[10];
                    verticalProgressBar5.Value = (int)rawState[13];

                    // right
                    verticalProgressBar6.Value = 0;
                    verticalProgressBar7.Value = 0;
                    verticalProgressBar8.Value = 0;
                    verticalProgressBar9.Value = 0;
                    verticalProgressBar10.Value = 0;
                }
                else
                {
                    // left
                    verticalProgressBar1.Value = 0;
                    verticalProgressBar2.Value = 0;
                    verticalProgressBar3.Value = 0;
                    verticalProgressBar4.Value = 0;
                    verticalProgressBar5.Value = 0;

                    // right
                    verticalProgressBar6.Value = (int)rawState[2];
                    verticalProgressBar7.Value = (int)rawState[5];
                    verticalProgressBar8.Value = (int)rawState[7];
                    verticalProgressBar9.Value = (int)rawState[10];
                    verticalProgressBar10.Value = (int)rawState[13];
                }
            }
        }

        private void PrefixText_TextChanged(object sender, EventArgs e)
        {
            dataPrefix = PrefixText.Text;
            CalibrationTitle.Text = "Participant: " + dataPrefix + " (Hold pose then press 'Start')"; ;
            measurementTitle.Text = "Participant: " + dataPrefix + " (Press 'Start' then move target finger)";
        }

        private void CompletionSounds_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void BlocksNumeric_ValueChanged(object sender, EventArgs e)
        {
            dataMaxStates = (int)BlocksNumeric.Value * (1000 / dataSampleInterval);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (MySerialPort.IsOpen)
            {
                Console.WriteLine("Closing serial ports ...");
                MySerialPort.Close();

                if(!MySerialPort.IsOpen)
                    Console.WriteLine("Serial ports closed.");
            }

            base.OnFormClosing(e);
        }

        private void LeftCheck_OnClick(object sender, EventArgs e)
        {
            handedness = Hands.L;
            Left1.Enabled = LeftCheck.Checked;
            Right1.Enabled = RightCheck.Checked;

            calPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + calibration.ToString() + ".png";
            measPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + measurement.ToString() + fingerness.ToString() + ".png";
        }

        private void RightCheck_OnClick(object sender, EventArgs e)
        {
            handedness = Hands.R;
            Left1.Enabled = LeftCheck.Checked;
            Right1.Enabled = RightCheck.Checked;

            calPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + calibration.ToString() + ".png";
            measPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + measurement.ToString() + fingerness.ToString() + ".png";
        }

        private void neutralRadioB_OnClick(object sender, EventArgs e)
        {
            calibration = CalType.neutral;
            calPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + calibration.ToString() + ".png";
        }
        private void flexRadioB_OnClick(object sender, EventArgs e)
        {
            calibration = CalType.flexion;
            calPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + calibration.ToString() + ".png";
        }
        private void extRadioB_OnClick(object sender, EventArgs e)
        {
            calibration = CalType.extension;
            calPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + calibration.ToString() + ".png";
        }

        private void fingerCheckT_OnClick(object sender, EventArgs e)
        {
            fingerness = Fingers.T;
            measPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + measurement.ToString() + fingerness.ToString() + ".png";
            dataChart.Series.Clear();
            dataChart.Legends.Clear();
        }
        private void fingerCheckI_OnClick(object sender, EventArgs e)
        {
            fingerness = Fingers.I;
            measPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + measurement.ToString() + fingerness.ToString() + ".png";
            dataChart.Series.Clear();
            dataChart.Legends.Clear();
        }
        private void fingerCheckM_OnClick(object sender, EventArgs e)
        {
            fingerness = Fingers.M;
            measPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + measurement.ToString() + fingerness.ToString() + ".png";
            dataChart.Series.Clear();
            dataChart.Legends.Clear();
        }
        private void fingerCheckR_OnClick(object sender, EventArgs e)
        {
            fingerness = Fingers.R;
            measPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + measurement.ToString() + fingerness.ToString() + ".png";
            dataChart.Series.Clear();
            dataChart.Legends.Clear();
        }
        private void fingerCheckP_OnClick(object sender, EventArgs e)
        {
            fingerness = Fingers.P;
            measPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + measurement.ToString() + fingerness.ToString() + ".png";
            dataChart.Series.Clear();
            dataChart.Legends.Clear();
        }

        private void measExtCheck_OnClick(object sender, EventArgs e)
        {
            measurement = MeasType.extension;
            measPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + measurement.ToString() + fingerness.ToString() + ".png";
            dataChart.Series.Clear();
            dataChart.Legends.Clear();
        }

        private void measFlexCheck_OnClick(object sender, EventArgs e)
        {
            measurement = MeasType.flexion;
            measPictureBox.ImageLocation = "./Assets/" + handedness.ToString() + measurement.ToString() + fingerness.ToString() + ".png";
            dataChart.Series.Clear();
            dataChart.Legends.Clear();
        }
    }


    public enum CalType
    {
        neutral,
        extension,
        flexion,
    }

    public enum Hands
    {
        L,
        R,
    }

    public enum MeasType
    {
        flexion,
        extension,
    }

    public enum Fingers
    {
        T,
        I,
        M,
        R,
        P,
    }

    public enum FingerNames
    {
        thumb,
        index,
        middle,
        ring,
        pinky,
    }

    public class GloveState
    {
        //public DateTime timeStamp { get; set; }
        public byte Tp { get; set; }
        public byte Tm { get; set; }
        public byte Td { get; set; }
        public byte TI { get; set; }
        public byte Ip { get; set; }
        public byte Im { get; set; }
        public byte IM { get; set; }
        public byte Mp { get; set; }
        public byte Mm { get; set; }
        public byte MR { get; set; }
        public byte Rp { get; set; }
        public byte Rm { get; set; }
        public byte RP { get; set; }
        public byte Pp { get; set; }
        public byte Pm { get; set; }
        public byte LA { get; set; }
        public byte Wf { get; set; }
        public byte Wd { get; set; }

        public GloveState(byte[] raw)
        {
            //timeStamp = DateTime.Now;
            Tp = raw[0];
            Tm = raw[1];
            Td = raw[2];

            Ip = raw[4];
            Im = raw[5];

            Mp = raw[6];
            Mm = raw[7];

            Rp = raw[9];
            Rm = raw[10];

            Pp = raw[12];
            Pm = raw[13];

            TI = raw[3];
            IM = raw[8];
            MR = raw[11];
            RP = raw[14];

            LA = raw[15];

            Wf = raw[16];
            Wd = raw[17];

            /*  
            1 - Thumb roll
            2 - Thumb inner
            3 - Thumb outer
            4 - Thumb-index abduction
            5 - Index inner
            6 - Index middle
            X - Index outer
            7 - Middle inner
            8 - Middle middle
            X - Middle outer
            9 - Index-Middle abduction
            10 - Ring inner
            11 - Ring middle
            X - Ring outer
            12 - Middle-Ring abduction
            13 - Pinky inner
            14 - Pinky middle
            X - Pinky outer
            15 - Ring-Pinky abduction
            16 - Palm arch
            17 - Wrist flexion
            18 - Wrist abduction
            19 - Unknown
            20 - Unknown
            21 - Unknown 
            */
        }
    }
}


