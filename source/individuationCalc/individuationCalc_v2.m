%  (c) 2023 AD Gomez
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

clc
close all
clear all

%% input

path = '.\sampleData';
prefix = 'TEST';
hand = 'L';

%% data extraction

formatHint = '.csv';
handHint = ['_' hand];
calHint = '_cal';
measHint = '_meas';

colNames = {'Tp','Tm','Td','TI','Ip','Im','IM','Mp','Mm','MR','Rp','Rm','RP','Pp','Pm','LA','Wf','Wd'};

% 01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18
% Tp,Tm,Td,TI,Ip,Im,IM,Mp,Mm,MR,Rp,Rm,RP,Pp,Pm,LA,Wf,Wd

colJoints = [2 5 8 11 14 3 6 9 12 15];


% dir structure
files = dir([path '\' prefix]);

% loop over all files
for cnt_j = 1:size(files,1)
    
    % current file
    filename = files(cnt_j).name;
        
    % remove indentifier
    k = strfind(filename,'_');
    
    if(~isempty(k))
       
        testname = filename(k(1):end);

        % look for format
        if(contains(testname, formatHint))

            % read data
            % DATA = table2array(readtable([path '\' prefix '\' filename],'NumHeaderLines',1));
            DATA = table2array(readtable([path '\' prefix '\' filename],'HeaderLines',1));

            % look for hand
            if(contains(testname, handHint))

                % look for calibration
                if(contains(testname, calHint))

                    if(contains(testname, "neutral"))
                        CALN = DATA;
                    elseif(contains(testname, "extension"))
                        CALE = DATA;
                    elseif(contains(testname, "flexion"))
                        CALF = DATA;
                    end

                % look for measurement
                elseif(contains(testname, measHint))

                    if(contains(testname, "extension"))

                        if(contains(testname, "T_"))    
                            JDE{1} = DATA;
                        elseif(contains(testname, "I_")) 
                            JDE{2} = DATA;
                        elseif(contains(testname, "M_")) 
                            JDE{3} = DATA;
                        elseif(contains(testname, "R_")) 
                            JDE{4} = DATA;
                        elseif(contains(testname, "P_")) 
                            JDE{5} = DATA;
                        end

                     elseif(contains(testname, "flexion"))

                        if(contains(testname, "T_"))    
                            JDF{1} = DATA;
                        elseif(contains(testname, "I_")) 
                            JDF{2} = DATA;
                        elseif(contains(testname, "M_")) 
                            JDF{3} = DATA;
                        elseif(contains(testname, "R_")) 
                            JDF{4} = DATA;
                        elseif(contains(testname, "P_")) 
                            JDF{5} = DATA;
                        end

                    end

                end
            end

        end
             
    end

end



%% calibration 

if(exist('CALN') && exist('CALE') && exist('CALF'))

    % calibration vars
    POSN = mean(CALN);
    POSE = mean(CALE);
    POSF = mean(CALF);

    % plots
    figure
    for cnt_j = 1:10
        n = colJoints(cnt_j);
        subplot(2,5,cnt_j)
        title([handHint(2) ' cal ' colNames{n}])
        hold on
        plot(CALN(:,n))
        plot(CALE(:,n))
        plot(CALF(:,n))  
        grid on
        axis([0 20 0 255])
    end

    legend('neutral','ext','flex')

else
    error('no calibration data')
end

%% individuation - flexion

% flexion
if(exist('JDF','var'))
    if(4 < size(JDF,2))
    
        % initialize coefficients gestures-by-joints
        SF = zeros(5,10);

        % initialize plots
        f1 = figure;
        f2 = figure;
        clines = lines;

        % loop over gestures
        for cnt_g = 1:5
    
            % normalization
            NDF{cnt_g} = (JDF{cnt_g} - repmat(POSN,[size(JDF{cnt_g},1) 1]))./ repmat((POSF - POSN),[size(JDF{cnt_g},1) 1]);
        
            % loop over joints
            for cnt_j = 1:5
    
                %  get proximal and distal joint indices
                ijp = colJoints(cnt_j);
                igp = colJoints(cnt_g);

                ijd = colJoints(cnt_j + 5);
                igd = colJoints(cnt_g + 5);

                % get coefficients 
                Pp = polyfit(NDF{cnt_g}(:,igp),NDF{cnt_g}(:,ijp),1);
                Pd = polyfit(NDF{cnt_g}(:,igd),NDF{cnt_g}(:,ijd),1);

                SF(cnt_g,cnt_j) = Pp(1);
                SF(cnt_g,cnt_j + 5) = Pd(1);
        
                figure(f1)
                subplot(5,2,2*cnt_g - 1)               
                hold on
                plot(NDF{cnt_g}(:,ijp),'color',clines(cnt_j,:))
                title([handHint(2) ' F ' colNames{igp} ' evo'])
                grid on

                subplot(5,2,2*cnt_g)               
                hold on
                plot(NDF{cnt_g}(:,ijd),'color',clines(cnt_j,:))
                title([handHint(2) ' F ' colNames{igd} ' evo'])
                grid on


                figure(f2)
                subplot(5,2,2*cnt_g - 1)                 
                hold on
                plot(NDF{cnt_g}(:,igp),Pp(2) + NDF{cnt_g}(:,igp)*Pp(1),'color',clines(cnt_j,:))
                plot(NDF{cnt_g}(:,igp),NDF{cnt_g}(:,ijp),'.','color',clines(cnt_j,:))
                title([handHint(2) ' F ' colNames{igp} ' fit'])
                grid on

                subplot(5,2,2*cnt_g)                 
                hold on
                plot(NDF{cnt_g}(:,igd),Pd(2) + NDF{cnt_g}(:,igd)*Pd(1),'color',clines(cnt_j,:))
                plot(NDF{cnt_g}(:,igd),NDF{cnt_g}(:,ijd),'.','color',clines(cnt_j,:))
                title([handHint(2) ' F ' colNames{igd} ' fit'])
                grid on

            end
        end

        figure(f1)
        legend('T','I','M','R','P')

        handHint
        IIFp = 1 - (sum(abs(SF(:,1:5)),2)-1)*0.25
        IIFd = 1 - (sum(abs(SF(:,6:10)),2)-1)*0.25

    else
        warning('missing flexion data')
    end
end

return



%% individuation - extension

% extension
if(exist('JDE','var'))
    if(4 < size(JDE,2))
    
        % initialize coefficients gestures-by-joints
        SE = zeros(5,10);

        % initialize plots
        f1 = figure;
        f2 = figure;
        clines = lines;

        % loop over gestures
        for cnt_g = 1:5
    
            % normalization
            NDE{cnt_g} = (JDE{cnt_g} - repmat(POSN,[size(JDE{cnt_g},1) 1]))./ repmat((POSF - POSN),[size(JDE{cnt_g},1) 1]);
        
            % loop over joints
            for cnt_j = 1:5
    
                %  get proximal and distal joint indices
                ijp = colJoints(cnt_j);
                igp = colJoints(cnt_g);

                ijd = colJoints(cnt_j + 5);
                igd = colJoints(cnt_g + 5);

                % get coefficients 
                Pp = polyfit(NDE{cnt_g}(:,igp),NDE{cnt_g}(:,ijp),1);
                Pd = polyfit(NDE{cnt_g}(:,igd),NDE{cnt_g}(:,ijd),1);

                SE(cnt_g,cnt_j) = Pp(1);
                SE(cnt_g,cnt_j + 5) = Pd(1);
        
                figure(f1)
                subplot(5,2,2*cnt_g - 1)               
                hold on
                plot(NDE{cnt_g}(:,ijp),'color',clines(cnt_j,:))
                title([handHint(2) ' E ' colNames{igp} ' evo'])
                grid on

                subplot(5,2,2*cnt_g)               
                hold on
                plot(NDE{cnt_g}(:,ijd),'color',clines(cnt_j,:))
                title([handHint(2) ' E ' colNames{igd} ' evo'])
                grid on


                figure(f2)
                subplot(5,2,2*cnt_g - 1)                 
                hold on
                plot(NDE{cnt_g}(:,igp),Pp(2) + NDE{cnt_g}(:,igp)*Pp(1),'color',clines(cnt_j,:))
                plot(NDE{cnt_g}(:,igp),NDE{cnt_g}(:,ijp),'.','color',clines(cnt_j,:))
                title([handHint(2) ' E ' colNames{igp} ' fit'])
                grid on

                subplot(5,2,2*cnt_g)                 
                hold on
                plot(NDE{cnt_g}(:,igd),Pd(2) + NDE{cnt_g}(:,igd)*Pd(1),'color',clines(cnt_j,:))
                plot(NDE{cnt_g}(:,igd),NDE{cnt_g}(:,ijd),'.','color',clines(cnt_j,:))
                title([handHint(2) ' E ' colNames{igd} ' fit'])
                grid on

            end
        end

        figure(f1)
        legend('T','I','M','R','P')

        handHint
        IIEp = 1 - (sum(abs(SE(:,1:5)),2)-1)*0.25

        IIEd = 1 - (sum(abs(SE(:,6:10)),2)-1)*0.25

    else
        warning('missing extension data')
    end
end


