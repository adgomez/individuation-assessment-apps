/*
(c) 2023 AD Gomez 

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


int PINT = 33;
int PINI = 34;
int PINM = 35;
int PINR = 36;
int PINP = 37;

int PINLED = 13;
bool ledstate = true;

int iter = 0;
const int L = 21; 
byte payload[L] = {0};


void setup() 
{
  Serial.begin(115200);
  
  pinMode(PINT, INPUT);
  pinMode(PINI, INPUT);
  pinMode(PINM, INPUT);
  pinMode(PINR, INPUT);
  pinMode(PINP, INPUT);

  pinMode(PINLED, OUTPUT); 
}

void loop() 
{
  // arbutrary data for last byte
  if (255 < iter)
    iter = 0;

  // T
  payload[0] = byte(analogRead(PINT)/4);
  payload[1] = byte(analogRead(PINT)/4);
  payload[2] = byte(analogRead(PINT)/4);
  
  // I       
  payload[4] = byte(analogRead(PINI)/4);  
  payload[5] = byte(analogRead(PINI)/4);

  // M
  payload[6] = byte(analogRead(PINM)/4);
  payload[7] = byte(analogRead(PINM)/4);

  // R
  payload[9] = byte(analogRead(PINR)/4);  
  payload[10] = byte(analogRead(PINR)/4);
  
  // P
  payload[12] = byte(analogRead(PINP)/4);
  payload[13] = byte(analogRead(PINP)/4);

  // ABD
  payload[3] = byte(128 + (analogRead(PINT) - analogRead(PINI))/8);
  payload[8] = byte(128 + (analogRead(PINI) - analogRead(PINM))/8);
  payload[11] = byte(128 + (analogRead(PINM) - analogRead(PINR))/8);
  payload[14] = byte(128 + (analogRead(PINR) - analogRead(PINP))/8); 

  // LA
  payload[15] = byte(10);

  // W
  payload[16] = byte(12);
  payload[17] = byte(50);

  // PACKET
  payload[18] = byte(60);
  payload[19] = byte(0);  
  payload[20] = byte(83);

  // handy timer
  if(iter%4 == 0)
  {
    ledstate = !ledstate;
    digitalWrite(PINLED, ledstate);
  }


  iter++;
  
  // send serial data
  bool humanReadable = false;
  
  if(humanReadable)
  {
    for (int i=0; i<L-1; i++)
    {
      Serial.print(payload[i]);
      Serial.print(",");
    }
    Serial.println(payload[L-1]);
  }
  else
  {
      Serial.write(payload, L);
  }

  // some sensible delay
  delay(10);
}
