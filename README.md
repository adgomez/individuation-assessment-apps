# Individuation Assessment Apps


This is a little compilation of sofware for hand dexterity assessment using the concept of [individuation](https://pubmed.ncbi.nlm.nih.gov/11069962/). This concept and corresponding metrics have been used in conjunction with a very old tech called [cybergloves](http://www.cyberglovesystems.com/) to understand hand function in [neurological patients](https://pubmed.ncbi.nlm.nih.gov/16207778/) and to examine the effectiveness of [interventions](https://pubmed.ncbi.nlm.nih.gov/25542201/). 

The purpose of this repository is to facilitate repeatibility studies using the semi-obsolete hardware using in 2023 computers. The information herein is a technical note intended only as a research tool.  This entire thing works on Windows 11. Because the legacy sofware needed to start the data stream, it would be very tricky to get everything to run in other operating systems.  


## Hardware

The pipeline was develped and tested using two 18 DOF cybergloves, an ADC model CGIU2402, and its power supply (specs can bee seen in the image).

![image](pictures/hardware_gloves.png "cyberglove hardware")

The ADC outputs a vintage, RS-232 compliant serial stream, so the first challenge is to pipe and read the stream using USB-C hardware. To this end, we need a a couple of adapters. The first and most important is a serial to USB adapter. Here, we used Prolific Smart-IO adapter whose drivers may be found [here](https://www.prolific.com.tw/US/ShowProduct.aspx?pcid=41&showlevel=0041-0041). The second adapter is a USB-A to USB-C--This is basically a form factor change leveraging USB backward compatibility. It is recommended that the later adapter supports USB 3.0.      

![image](pictures/hardware_adapters.png "from RS-232 to USB-C")

Once the adapter's drivers have been installed, the old [Devices and Printers](https://www.top-password.com/blog/open-the-devices-and-printers-in-windows-10/) utility can be used to verfy that everything works. 

![image](pictures/hardware_dev_prin.png "devices and printers")

Not all serial to USB adapters remain compatible with Windows 11. If the device has a problem, a small icon will appear next to the icon representing the device. If that is the case, the driver may need to be reinstalled (the PC will most likely need to restart afterwards), or a different adapter may be needed. 



## Software

There are three software aspects to getting the assessment up and running. The first is starting the data stream from the ADC to the PC. The second is accessing the stream, saving hand pose data when needed and verifying that the data is valid. Lastly, we need to run a script to calculate the actual individuation index for each finger. Each of these aspects involves separate sofware which will be discussed below:   


**Starting the stream:** The Cyberglove hardware requires two binaries to activate the stream. Once the stream is active, it will remain that way until the computer is restarted or shut off. These binaries are part of the Cyberglove SDK package with an installer called "VirtualHand SDK V2.9.msi" which seems to still be distributed in some places across the web and can be found if one searches for it ;) The binaries are called the "Device Manager" (which is confusing given Windows onw untility by the same name) and "DCU". The first step to activate the stream is to simply run the Device Manager. The second step is to open the DCU application, set a configuration, set a glove device, and connect to the glove device. After the glvoe is connected, the ADC stream will be activated. Thereafter, one can close both applications. 

_NOTE: The DCU port naming is limited to four serial communication ports, COM 1 thru COM 4 (see Devices and Printers). If the serial-to-USB adapter has a port name different different than those, they would have to be renamed. Otherwise, the communication ports will not be accessed by the DCU  and the stream will not be activated._ 


**Data acquisition:** Once the cyberglove data stream has been activated, the assessment may be carried out. The data aqcusition (DAQ) application (source included in this repository) is used to name and save the corresponding posture data. There are some steps necessary to handle the stream to prevent errors, but in essence, the DAQ app simply opens the port and saves the data to as csv file. The app is a simple Windows form in C#. In a research setting, the actual individuation assessment would ideally be performed by a licensed therapist. 

The first step is to select the communications port and enter the participant's identifier and target hand. An option for hearing sounds at the start and stop of each acquisition is given along with a timeout period after wich the acquisition will end automatically. 

![image](pictures/software_daq_intro.png "DAQ settings")

Once the settings are complete, one can navigate to the Calibration tab. There, one can select the desired calibration posture and press 'Start Calibration' while the participant is holding the posture. After a few seconds of holding, the acquisition may be completed by pressing the 'Stop Calibration' button. Vertical bars for each finger are provided as a visual verification of the data. During flexion the bars should be near full and extension should result in mostly empty bars. When neutral, the bars should be approximately half the way full. The measurement may be repeated as necessary.

![image](pictures/software_daq_cal.png "DAQ calibration")

After calibration, one can navigate to the Individuation tab. Data identification and saving is identical to the calibration process. However, during individuation the poses are not held in place; instead, the instructed finger is flexed or extended several times as the data is saved. Once the acquisition has concluded, a chart tracing the medial joints of each finger will appear. In general, but not always, the instructed finger will register more movement than the others. The chart can provide visual verificaiton of the data quality. The measurement may be repeated as necessary.

![image](pictures/software_daq_ind.png "DAQ individuation")
  
Lastly, the app will show a summary of the data acquired so far. A check mark is added to the pose after the corresponding data has been saved. The list will not update if an aqcusition is to be repeated. Instead, the checkmarks can be undone manually, although that is neither necessary nor recommended. 

![image](pictures/software_daq_sum.png "DAQ summary")  

All data will be saved in a folder corresponding to the participant ID. This folder is created automatically by the app in the same location as the binary running it.   


**Individuation calculation:** Once the raw data has been acquired and verified, the individuation indices may be calculated using the included MATLAB script. The script was tested in MATLAB 2018a and 2019b, but some minor syntax updates not covered here may be needed for older or newer versions. As an input, the script will require a path corresponding to the location of the raw data, a prefix corresponding to the participant identifier, and a handedness flag 'L' for left and 'R' for right. With this information, the script will automatically extract the required data or provide an error with some feedback about what data is missing. If all the data is complete, the script will provide as an output figures about the  data and the extension (or flexion) individuation indices for each finger. Some of the output associated with the sample data set included with in this repository is as follows: 

The figure below shows calibration data for two fingers in the left hand. Information about the source of the data is encoded in the title e.g., __L cal Pm__ corresponds to left (L) calibration (cal) of the pinky's medial joint (Pm). The figures show a time history of the raw sensor output in arbitrary units. The value is proportional to the amount of flexion and inversely proportional to the amount of extension. The neutral pose is assumed to have zero flexion or extension and corresponds to a value between the extremes. After calibration, values will correspond to the approximate percentage from full extension (-100%) to full flexion (100) regardless of the arbitrary units. Note for instance the differences in arbitrary units between the medial joints in the pinky (A) and the index finger (B).

![image](pictures/data_cal.png "Calibration data, left hand, pinky (A) and index (B)")   

In flexion, our test participant exhibited less individuation in the pinky compared to the index finger. As a result, the time history of the pinky's flexion (A in the image shown below) suggests movement of the adjacent joints in addition to the instructed finger. Note incidentally that the amount of flextion of the instruced joint reaches a maxium beyond 1.0 (or 100%). This simply means that the flexion during the measurement was deeper than that observed during calibration. This has little to no effect in the individuation results. Note also that, unlike calibration which was held in place, the time history of the measurement is a dynamic waveform because the pose was repeated several times as the data was acquired. The script also shows a scatter plot of the movement of the instructed finger (x-axis) with respect to each finger (y-axis) (B in the image below). This means that a perfect slope of one appears as a fit of the movement of the instructed finger against itself e.g, the pinky trace in the image. In a finger with low individuation index, the adjacent fingers will tend to produce slopes near 1.0 (as opposed to zero). The individuation coefficient (II) works in the opposite way. That is, a number near 1.0 represents high individuation while a value near zero means the movement of the instructed finger is associated with movement of the adjacent fingers. of the pinky is 0.666 in the data shown.

![image](pictures/data_lowII.png "II of 0.666, time history and linear fit")   

In contrast to the pinky above, the index finger had a much higher II at 0.997. Visual inspection of the data reveals an opposite trend comparatively. The time history (A in the image below) shows ample flexion and even some extension while the adjacent fingers exhibit minimum movement. Correspondinly, the fit graph (B in the image below) suggests that movement of the index is largely independent form the other fingers. 

![image](pictures/data_highII.png "II of 0.997, time history and linear fit")   

The script will return a text output with the IIs of each finger (two joints each) in the following order: thumb, index, middle, ring, and pinky. The more distal joints have a suffix 'd' and the more proximal ones are labeled with a 'p' at the end.


## Future Hardware Alterntatives

Lastly, this repository includes an arduino-bases emulator of the cyberglove in C++. Unlike the real thing which requires the SDK and cumbersome stream activation above. The emulator will be active in any port and uses the hardware ADC to read a a [flex sensor](https://en.wikipedia.org/wiki/Flex_sensor) connected to a voltage divider. This software will enable a backward-compatible usage of the DAQ routines if a custom glove is manufactured. Instructions to acomplish this are not included here but may be found online or deducted intuitively. 


## About

(c) 2023 by AD Gomez

Permission is given to use the images herein for educational, research, and other non-commercial purposes. 

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


<!---```
ADG 2023
```-->